# https://note.nkmk.me/python-csv-reader-writer/
# https://docs.python.org/ja/3/library/csv.html
# https://baijiahao.baidu.com/s?id=1690115862241697671&wfr=spider&for=pc
# https://www.runoob.com/pandas/pandas-json.html
# http://c.biancheng.net/pandas/series.html

from configparser import RawConfigParser
from csv import QUOTE_MINIMAL, DictWriter, reader, writer

print("hello")


def index_multi(str, x):
    return [i for i, _x in enumerate(str) if _x == x]


def write_csv():
    # l1 = {'dept': 'deptA', 'type': '登録件数', '記事': 1, '前文': '1', '合計': 2}
    # l2 = {'dept': 'deptB', 'type': '登録件数', '記事': 2, '前文': '2', '合計': 4}
    # l3 = {'dept': 'deptC', 'type': 'リリース件数', '記事': 3, '前文': '3', '合計': 6}
    # l4 = {'dept': 'deptD', 'type': 'リリース件数', '記事': 4, '前文': '4', '合計': 8}
    # l11 = {'dept': 'deptA', 'type': '登録件数', '記事': 11, '前文': '11', '合計': 22}
    # l22 = {'dept': 'deptB', 'type': '登録件数', '記事': 22, '前文': '22', '合計': 44}
    # l33 = {'dept': 'deptC', 'type': 'リリース件数', '記事': 33, '前文': '33', '合計': 66}
    # l44 = {'dept': 'deptD', 'type': 'リリース件数', '記事': 44, '前文': '44', '合計': 88}
    # l55 = {'dept': 'deptE', 'type': 'リリース件数', '記事': 55, '前文': '55', '合計': 110}

    
    l1 = {'dept|$|deptA': 'deptA', 'type': '登録件数', '記事': 1, '前文': '1', '合計': 2}
    l2 = {'dept|$|deptB': 'deptB', 'type': '登録件数', '記事': 2, '前文': '2', '合計': 4}
    l3 = {'dept|$|deptC': 'deptC', 'type': 'リリース件数', '記事': 3, '前文': '3', '合計': 6}
    l4 = {'dept|$|deptD': 'deptD', 'type': 'リリース件数', '記事': 4, '前文': '4', '合計': 8}
    l11 = {'dept|$|deptA': 'deptA', 'type': '登録件数', '記事': 11, '前文': '11', '合計': 22}
    l22 = {'dept|$|deptB': 'deptB', 'type': '登録件数', '記事': 22, '前文': '22', '合計': 44}
    l33 = {'dept|$|deptC': 'deptC', 'type': 'リリース件数', '記事': 33, '前文': '33', '合計': 66}
    l44 = {'dept|$|deptD': 'deptD', 'type': 'リリース件数', '記事': 44, '前文': '44', '合計': 88}
    l55 = {'dept|$|deptE': 'deptE', 'type': 'リリース件数', '記事': 55, '前文': '55', '合計': 110}
    order = {
        'dept': 0,
        'deptC': 3,
        'deptD': 2,
        'deptE': 1,
    }
    # header = ['dept', 'type', '記事', '前文', '合計']
    header = ['dept', 'type', '記事', '前文', '合計']
    content = [l1, l2, l3, l4, l11, l22, l33, l44]
    with open('test1.csv', 'w', encoding='UTF-8', newline='') as csvfile:
        w = DictWriter(csvfile, fieldnames=header)
        w.writeheader()
        w.writerows(content)
        w.writerow(l55)

    # 合わせ
    merge_list = {}
    with open('test1.csv', 'r', encoding='UTF-8', newline='') as f:
        for line in f:
            key = line[:index_multi(line, ',')[1]]
            # line = line.replace('\r\n', '')
            if key in merge_list:
                new_list = []
                for i, item in enumerate(line.split(',')):
                    try:
                        item = int(item) + int(merge_list[key].split(',')[i])
                    except Exception:
                        item = item
                    new_list.append(str(item))
                merge_list[key] = ','.join(new_list) + '\r\n'
            else:
                merge_list[key] = line
    print(merge_list)
    # print(list(merge_list.values()))
    with open('test1.csv', 'w', encoding='UTF-8', newline='') as ff:
        for line in list(merge_list.values()):
            ff.writelines(line)
    total_lines = 0
    with open('test1.csv', 'r', encoding='UTF-8', newline='') as sum_f:
        total_lines = sum(1 for item in sum_f)
    # 並び
    order_list = []
    with open('test1.csv', 'r', encoding='UTF-8', newline='') as fff:
        for line in fff:
            try:
                key = order[line[:index_multi(line, ',')[0]]]
            except KeyError:
                total_lines += 1
                key = total_lines
            order_list.append({'order': key, 'data': line})

    print(order_list)
    # for line in sorted(order_list, key=lambda x: x['order']):
    #     print(line['data'])
    with open('test1.csv', 'w', encoding='UTF-8', newline='') as ff:
        for line in sorted(order_list, key=lambda x: x['order']):
            ff.writelines(line['data'])


def read_properties():
    config = RawConfigParser()
    config.read('path_to_config.properties')
    details_dict = dict(config.items('SECTION_NAME'))
    print(details_dict)
    list = details_dict['key1'].split(',') + details_dict['key2'].split(',')
    print(list)


write_csv()
read_properties()