import requests
from bs4 import BeautifulSoup
import csv
import sys

#検索する単語を入力
word = input("search word:")

#単語が保存されているリストファイルを開く
try:
    with open('list.csv') as f:
        reader = csv.reader(f)
        l = [row for row in reader]

except:
    print("Listfile doesn't exist")
    exit(1)

#すでに保存されている単語かどうかチェック
for exist_word in l:
    if exist_word[0] == word:
        exist_word_verif = input("This word is already registered. Check mean? y/n:")
        if exist_word_verif == "n":
            sys.exit(0)
        elif exist_word_verif == "y":
            print(exist_word[1])
            sys.exit(0)
        else:
            print("Please input y or n. Start again.")
            sys.exit(1)

#urlを検索・スクレイピング
search_word = "https://ejje.weblio.jp/content/" + word

try:
    url = requests.get(search_word)
    soup = BeautifulSoup(url.text, "html.parser")
    word_mean = soup.find(class_='content-explanation ej').get_text()

except:
    print("This word doesn't exist. Start again.")
    exit(1)

#単語の意味を表示
print(word_mean)

#リストファイルに追加するかどうか選択
append_or_not = input("append y/n:")

if append_or_not == "y":
    data = [word, word_mean]
    with open('list.csv', 'a') as f:
        writer = csv.writer(f, lineterminator='\n')
        writer.writerow(data)