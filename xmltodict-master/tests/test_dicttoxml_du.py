from xmltodict import parse, unparse, ParsingInterrupted
import collections
import unittest
from collections import OrderedDict
from textwrap import dedent

try:
    from io import BytesIO as StringIO
except ImportError:
    from xmltodict import StringIO

from xml.parsers.expat import ParserCreate
from xml.parsers import expat


def _encode(s):
    try:
        return bytes(s, 'ascii')
    except (NameError, TypeError):
        return s


class XMLToDictTestCase(unittest.TestCase):

    def test_string_vs_file(self):
        xml = """
        <root>


          <emptya>           </emptya>
          <emptyb attr="attrvalue">


          </emptyb>
          <value>hello</value>
        </root>
        """
        print (parse(xml))
        print (parse(StringIO(_encode(xml))))
        # self.assertEqual(parse(xml),
        #                  parse(StringIO(_encode(xml))))
        obj = {'a': OrderedDict((
            ('b', [{'c': [1, 2]}, 3]),
            ('x', 'y'),
        ))}
        newl = '\n'
        indent = '....'
        xml = dedent('''\
        <?xml version="1.0" encoding="utf-8"?>
        <a>
        ....<b>
        ........<c>1</c>
        ........<c>2</c>
        ....</b>
        ....<b>3</b>
        ....<x>y</x>
        </a>''')
        print (xml)
        print (unparse(obj, pretty=True,
                                      newl=newl, indent=indent))

        s = 'deep insider'
        a = 10

        print(s)  # deep insider：変数の値を出力
        print(100)  # 100：整数リテラルを出力
        print('deep insider')  # deep insider：文字列リテラルを出力
        print([s, a])  # ['deep insider', 10]：リストを出力
        print(2 * a)  # 20：式の計算結果を出力

        # 複数の値を空白文字で区切って出力
        print(s, a)  # deep insider 10

        # 末尾の改行文字を出力しない
        print(s, end='')  # deep insider（改行なし）：キーワード引数endに空文字列を指定

        # 複数の値を区切る文字を空白文字から別の文字に変更する
        print(s, a, sep=',')  # deep insider,10：キーワード引数sepにカンマ「,」を指定

        # 変数や式の値を文字列に埋め込んで出力（出力は全て「value: 10」）
        print(f'value: {a}')  # f文字列を使用（Python 3.6以降）
        print('value: {}'.format(a))  # 文字列のformatメソッドを使用
        print('value: %d' % a)  # %演算子を使用
